#include "math.h"
#include "cgen.h"
#include "c2000.h"
#include "canBus.h"

int receive(CAN_RECEIVE canRec2){
	ADCTRL2 |= 0x4040;  // Reset ADC Seq
 	ADCTRL2 |= 0x2020;  // Trigger ADC
  	canRec2.messageArrived = TRUE;
  	if (CANRMPB&0x20)
		ecan_read_mbox(1, &canRec2 );
  
  	endOfSampleCount = TIMER2TIM;
  	
  	return canRec2.out1;
  	
}
