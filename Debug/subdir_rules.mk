################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
CanTransmit.obj: ../CanTransmit.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: Compiler'
	"C:/Program Files (x86)/Texas Instruments/C2000 Code Generation Tools 5.2.6/bin/cl2000" --silicon_version=28 -g --define=_DSP --define=_F28XX_ --define=_F280X_ --include_path="C:/Program Files (x86)/Texas Instruments/C2000 Code Generation Tools 5.2.6/include" --include_path="/vissim80/vsdk/include" --include_path="/vissim80/cg/include" --diag_warning=225 --large_memory_model --unified_memory --float_support=fpu32 --preproc_with_compile --preproc_dependency="CanTransmit.pp" $(GEN_OPTS_QUOTED) $(subst #,$(wildcard $(subst $(SPACE),\$(SPACE),$<)),"#")
	@echo 'Finished building: $<'
	@echo ' '

RecLib.obj: ../RecLib.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: Compiler'
	"C:/Program Files (x86)/Texas Instruments/C2000 Code Generation Tools 5.2.6/bin/cl2000" --silicon_version=28 -g --define=_DSP --define=_F28XX_ --define=_F280X_ --include_path="C:/Program Files (x86)/Texas Instruments/C2000 Code Generation Tools 5.2.6/include" --include_path="/vissim80/vsdk/include" --include_path="/vissim80/cg/include" --diag_warning=225 --large_memory_model --unified_memory --float_support=fpu32 --preproc_with_compile --preproc_dependency="RecLib.pp" $(GEN_OPTS_QUOTED) $(subst #,$(wildcard $(subst $(SPACE),\$(SPACE),$<)),"#")
	@echo 'Finished building: $<'
	@echo ' '

SendLib.obj: ../SendLib.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: Compiler'
	"C:/Program Files (x86)/Texas Instruments/C2000 Code Generation Tools 5.2.6/bin/cl2000" --silicon_version=28 -g --define=_DSP --define=_F28XX_ --define=_F280X_ --include_path="C:/Program Files (x86)/Texas Instruments/C2000 Code Generation Tools 5.2.6/include" --include_path="/vissim80/vsdk/include" --include_path="/vissim80/cg/include" --diag_warning=225 --large_memory_model --unified_memory --float_support=fpu32 --preproc_with_compile --preproc_dependency="SendLib.pp" $(GEN_OPTS_QUOTED) $(subst #,$(wildcard $(subst $(SPACE),\$(SPACE),$<)),"#")
	@echo 'Finished building: $<'
	@echo ' '

Setup.obj: ../Setup.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: Compiler'
	"C:/Program Files (x86)/Texas Instruments/C2000 Code Generation Tools 5.2.6/bin/cl2000" --silicon_version=28 -g --define=_DSP --define=_F28XX_ --define=_F280X_ --include_path="C:/Program Files (x86)/Texas Instruments/C2000 Code Generation Tools 5.2.6/include" --include_path="/vissim80/vsdk/include" --include_path="/vissim80/cg/include" --diag_warning=225 --large_memory_model --unified_memory --float_support=fpu32 --preproc_with_compile --preproc_dependency="Setup.pp" $(GEN_OPTS_QUOTED) $(subst #,$(wildcard $(subst $(SPACE),\$(SPACE),$<)),"#")
	@echo 'Finished building: $<'
	@echo ' '

f28335lnk.out: ../f28335lnk.cmd $(GEN_CMDS)
	@echo 'Building file: $<'
	@echo 'Invoking: Linker'
	"C:/Program Files (x86)/Texas Instruments/C2000 Code Generation Tools 5.2.6/bin/cl2000" --silicon_version=28 -g --define=_DSP --define=_F28XX_ --define=_F280X_ --diag_warning=225 --large_memory_model --unified_memory --float_support=fpu32 -z -m"GRRRR.map" --stack_size=0x300 --warn_sections -i"C:/Program Files (x86)/Texas Instruments/C2000 Code Generation Tools 5.2.6/lib" -i"/vissim80/cg/lib" -i"/vissim80/cg" -i"C:/Program Files (x86)/Texas Instruments/C2000 Code Generation Tools 5.2.6/include" --reread_libs --rom_model -o "$@" "$<" "../f28335lnk.cmd"
	@echo 'Finished building: $<'
	@echo ' '


